﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;

namespace ListAssemblyRefTree {
    class Program {
        static FileInfo assemblyFile;

        static void Main(string[] args) {
            if (args.Length != 1) {
                Console.WriteLine("Usage: ListAssemblyRefTree.exe pathtoassembly.dll");
                return;
            }

            try {
                setConsoleSize();
            } catch (Exception ex) { }


            assemblyFile = new FileInfo(args[0]);

            //try {
            //    Directory.SetCurrentDirectory(assemblyFile.Directory.FullName);
            //} catch (Exception ex) { }


            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.AssemblyResolve += new ResolveEventHandler(MyResolveEventHandler);


            Assembly asm = Assembly.LoadFile(assemblyFile.FullName);

            Stack<String> stack = new Stack<String>();

            ListRef(asm, "", stack);
        }

        static void ListRef(Assembly asm, string depth, Stack<String> stack) {
            stack.Push(asm.FullName);

            AssemblyName[] asmNames = asm.GetReferencedAssemblies();

            foreach (AssemblyName asmName in asmNames) {
                try {

                    Console.WriteLine(depth + asmName.FullName);

                    if (stack.Contains(asmName.FullName))
                        continue;
                    if (asmName.FullName.StartsWith("System"))
                        continue;
                    if (asmName.FullName.StartsWith("mscorlib"))
                        continue;

                    Assembly subasm = Assembly.Load(asmName);

                    ListRef(subasm, depth + "  ", stack);

                } catch (Exception ex) {
                    Console.Error.WriteLine(ex);
                }
            }

            stack.Pop();
        }

        static void setConsoleSize() {
            System.Console.SetWindowPosition(0, 0);   // sets window position to upper left
            System.Console.SetBufferSize(200, 300);   // make sure buffer is bigger than window
            System.Console.SetWindowSize(122, 54);   //set window size to almost full screen 
            //width - maxSet(127,57) (width, height)

            //System.Console.ResetColor(); //resets fore and background colors to default

        }  // End  setConsoleSize()

        static Assembly MyResolveEventHandler(object sender, ResolveEventArgs args) {
            //This handler is called only when the common language runtime tries to bind to the assembly and fails.

            //Retrieve the list of referenced assemblies in an array of AssemblyName.
            Assembly MyAssembly = null;

            string assemblyFilename = args.Name.Substring(0, args.Name.IndexOf(",")) + ".dll";
            
            FileInfo MYassemblyFile = new FileInfo(assemblyFilename);

            if (!MYassemblyFile.Exists)
                MYassemblyFile = new FileInfo(
                    Path.Combine(assemblyFile.Directory.FullName, assemblyFilename));

            try {
                //Load the assembly from the specified path. 					
                MyAssembly = Assembly.LoadFrom(MYassemblyFile.FullName);
            } catch (Exception ex) { }

            //Return the loaded assembly.
            return MyAssembly;
        }

    }


}

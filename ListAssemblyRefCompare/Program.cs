﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;

namespace ListAssemblyRefCompare {
    class Program {
        static void Main(string[] args) {
            if (args.Length != 2) {
                Console.WriteLine("Usage: ListAssemblyRefCompare.exe pathtoassembly1.dll pathtoassembly2.dll");
                return;
            }

            try {
                setConsoleSize();
            } catch (Exception ex) { }

            string assembly1 = preparePath(args[0]);
            string assembly2 = preparePath(args[1]);
            
            List<AssemblyEntry> assemblyList1 = getassemblys(assembly1);
            List<AssemblyEntry> assemblyList2 = getassemblys(assembly2);

            List<AssemblyEntry> toRemove1 = new List<AssemblyEntry>();
            List<AssemblyEntry> toRemove2 = new List<AssemblyEntry>();

            ////////////////////////////////////////////////////////////////////////
            //Find Full Matches
            Console.WriteLine("Full Matches:");

            foreach (AssemblyEntry ass1 in assemblyList1) {
                foreach (AssemblyEntry ass2 in assemblyList2) {
                    if (ass1.FullMatch(ass2)) {
                        toRemove1.Add(ass1);
                        toRemove2.Add(ass2);
                        Console.WriteLine("   " + ass1.ToString());
                    }
                }
            }
            foreach (var ass in toRemove1) assemblyList1.Remove(ass); toRemove1.Clear();
            foreach (var ass in toRemove2) assemblyList2.Remove(ass); toRemove2.Clear();
            ////////////////////////////////////////////////////////////////////////
            //Find Name Matches
            Console.WriteLine("Name Matches:");
            foreach (AssemblyEntry ass1 in assemblyList1) {
                foreach (AssemblyEntry ass2 in assemblyList2) {
                    if (ass1.AssemblyMatch(ass2)) {
                        toRemove1.Add(ass1);
                        toRemove2.Add(ass2);
                        Console.WriteLine("   " + ass1.ToString());
                        Console.WriteLine("   " + ass2.ToString());
                        Console.WriteLine();
                    }
                }
            }
            foreach (var ass in toRemove1) assemblyList1.Remove(ass); toRemove1.Clear();
            foreach (var ass in toRemove2) assemblyList2.Remove(ass); toRemove2.Clear();
            ////////////////////////////////////////////////////////////////////////

            Console.WriteLine("Unmatched Assembly 1:");
            foreach (var ass in assemblyList1) Console.WriteLine("   " + ass.ToString());

            Console.WriteLine("Unmatched Assembly 2:");
            foreach (var ass in assemblyList2) Console.WriteLine("   " + ass.ToString());
        }

        static List<AssemblyEntry> getassemblys(string assemblypath) {
            var proc = new Process {
                StartInfo = new ProcessStartInfo {
                    FileName = "ListAssemblyRefList.exe",
                    Arguments = assemblypath,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };

            List<AssemblyEntry> asslist = new List<AssemblyEntry>();

            proc.Start();
            while (!proc.StandardOutput.EndOfStream) {
                string line = proc.StandardOutput.ReadLine();
                asslist.Add(new AssemblyEntry(line));
            }
            proc.WaitForExit();

            return asslist;
        }

        static string preparePath(string path) {
            path = Path.GetFullPath(path);
            path = "\"" + path + "\"";
            return path;
        }

        static void setConsoleSize() {
            System.Console.SetWindowPosition(0, 0);   // sets window position to upper left
            System.Console.SetBufferSize(200, 300);   // make sure buffer is bigger than window
            System.Console.SetWindowSize(122, 54);   //set window size to almost full screen 
            //width - maxSet(127,57) (width, height)

            //System.Console.ResetColor(); //resets fore and background colors to default

        }  // End  setConsoleSize()
    }
}

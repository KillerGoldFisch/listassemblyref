﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ListAssemblyRefCompare {
    public class AssemblyEntry : IComparable<AssemblyEntry> {
        static Regex regex = new Regex(
            @"(?<name>[^,]*), Version=(?<version>[^,]*), Culture=(?<culture>[^,]*), PublicKeyToken=(?<token>[^,]*), Found=(?<found>[^,]*)");

        string name;
        string version;
        string culture;
        string token;
        string found;

        string fullName;


        public AssemblyEntry(string fullAssemblyName) {
            this.fullName = fullAssemblyName;

            Match match = regex.Match(fullAssemblyName);

            this.name = match.Groups["name"].Value;
            this.version = match.Groups["version"].Value;
            this.culture = match.Groups["culture"].Value;
            this.token = match.Groups["token"].Value;
            this.found = match.Groups["found"].Value;
        }

        public bool AssemblyMatch(AssemblyEntry other) {
            return this.name == other.name;
        }

        public bool FullMatch(AssemblyEntry other) {
            return this.name == other.name && this.version == other.version;
        }

        public override string ToString() {
            return this.fullName;
            //return string.Format("{0}, Version={1}, Culture={2}, PublicKeyToken={3}, Found={4}",
            //    this.name,
            //    this.version,
            //    this.culture,
            //    this.token,
            //    this.found
            //    );
        }

        public int CompareTo(AssemblyEntry other) {
            return this.name.CompareTo(other.name);
        }
    }
}

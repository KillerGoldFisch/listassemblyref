﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;

namespace ListAssemblyRefList {
    class Program {
        static FileInfo assemblyFile;

        static List<string> assemblyList = new List<string>();

        static void Main(string[] args) {
            if (args.Length != 1) {
                Console.WriteLine("Usage: ListAssemblyRefList.exe pathtoassembly.dll");
                return;
            }

            try {
                setConsoleSize();
            } catch (Exception ex) { }


            assemblyFile = new FileInfo(args[0]);

            //try {
            //    Directory.SetCurrentDirectory(assemblyFile.Directory.FullName);
            //} catch (Exception ex) { }


            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.AssemblyResolve += new ResolveEventHandler(MyResolveEventHandler);


            Assembly asm = Assembly.LoadFile(assemblyFile.FullName);

            Stack<String> stack = new Stack<String>();

            ListRef(asm, "", stack);

            assemblyList = assemblyList.Distinct().ToList();
            assemblyList.Sort();

            foreach (string ass in assemblyList)
                Console.WriteLine(ass);
        }

        static void ListRef(Assembly asm, string depth, Stack<String> stack) {
            stack.Push(asm.FullName);

            AssemblyName[] asmNames = asm.GetReferencedAssemblies();

            bool found = false;

            foreach (AssemblyName asmName in asmNames) {
                found = false;
                try {
                    //Console.WriteLine(depth + asmName.FullName);

                    if (stack.Contains(asmName.FullName))
                        continue;
                    if (asmName.FullName.StartsWith("System"))
                        continue;
                    if (asmName.FullName.StartsWith("mscorlib"))
                        continue;

                    Assembly subasm = Assembly.Load(asmName);
                    found = true;

                    ListRef(subasm, depth + "  ", stack);

                } catch (Exception ex) {
                    Console.Error.WriteLine(ex);
                }

                assemblyList.Add(asmName.ToString() + ", Found=" + found.ToString());
            }

            stack.Pop();
        }

        static void setConsoleSize() {
            System.Console.SetWindowPosition(0, 0);   // sets window position to upper left
            System.Console.SetBufferSize(200, 300);   // make sure buffer is bigger than window
            System.Console.SetWindowSize(122, 54);   //set window size to almost full screen 
            //width - maxSet(127,57) (width, height)

            //System.Console.ResetColor(); //resets fore and background colors to default

        }  // End  setConsoleSize()

        static Assembly MyResolveEventHandler(object sender, ResolveEventArgs args) {
            //This handler is called only when the common language runtime tries to bind to the assembly and fails.

            //Retrieve the list of referenced assemblies in an array of AssemblyName.
            Assembly MyAssembly = null;

            string assemblyFilename = args.Name.Substring(0, args.Name.IndexOf(",")) + ".dll";

            FileInfo MYassemblyFile = new FileInfo(assemblyFilename);

            if (!MYassemblyFile.Exists)
                MYassemblyFile = new FileInfo(
                    Path.Combine(assemblyFile.Directory.FullName, assemblyFilename));
            else {
                //Console.Error.WriteLine(string.Format("Assembly not found: {0}", args.Name));
            }

            try {
                //Load the assembly from the specified path. 					
                MyAssembly = Assembly.LoadFrom(MYassemblyFile.FullName);
            } catch (Exception ex) {
                Console.Error.WriteLine(ex);
            }

            //Return the loaded assembly.
            return MyAssembly;
        }

    }


}
